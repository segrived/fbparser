require 'nokogiri'
require 'open-uri'
require 'yaml'
require 'sequel'

config = YAML.load_file('config.yml')

db = Sequel.connect("sqlite://#{config['db']}")

class Sequel::Dataset
    def insert_ignore(values)
      sql = self.insert_sql(values)
      sql.gsub!(/^INSERT/, 'INSERT OR IGNORE')
      with_sql_insert sql
  end
end

class TeamMatchResult
  attr_accessor :team
  attr_accessor :result
end

class MatchResult
  attr_accessor :left
  attr_accessor :right

  def initialize
    @left  = TeamMatchResult.new
    @right = TeamMatchResult.new
  end

  def to_s
    "#{left.team} (#{left.result}:#{right.result}) #{right.team}"
  end
end

dataset = db[:teams]

file = File.open("results.yml", "w")
url = "http://football.ua/germany/results/302/118/"
page = Nokogiri::HTML(open(url))
matches = page.css('div.match')
results = matches.map do |m|
  match = MatchResult.new
  match.left.team = m.css('.left-team a').inner_text
  match.right.team = m.css('.right-team a').inner_text
  score_text = m.css('.score a').inner_text
  match.left.result, match.right.result = score_text.split(':').map(&:to_i)
  dataset.insert_ignore({ name: match.left.team })
  dataset.insert_ignore({ name: match.right.team })
end


file.write(results.to_yaml)
file.close
puts YAML::load(File.open("results.yml"))